Quarkus Kafka/Panache Quickstart
================================

This project illustrates how you can interact with Apache Kafka using MicroProfile Reactive Messaging and Hibernate with Panache.
The project uses:

* RESTEasy Reactive
* Reactive Messaging and its connector for Kafka
* (Classic) Hibernate with Panache

_NOTE:_ The [kafka-panache-reactive-quickstart](../kafka-panache-reactive-quickstart) provides the same example but using Hibernate Reactive.

## Start the application

The application can be started using: 

```bash
./mvnw quarkus:dev
```

_NOTE:_ The database and Kafka broker are started using Dev Services

Then, open your browser to `http://localhost:8080/prices`, and you should get the set of prices written in the database.
Every 5 seconds, a new price is generated, sent to a Kafka topic, received by a Kafka consumer and written to the database.
Refresh the page to see more prices.

## Running tests with Testcontainers

Quarkus employs Testcontainers (https://www.testcontainers.org/)
* Testcontainers starts some predefined docker containers, depending on the quarkus extensions used in a project, in this project Kafka
* Testcontainers uses redpanda kafka image, we override the image version at [src/main/resources/application.properties]():
    ```properties
    %test.quarkus.kafka.devservices.image-name=docker.vectorized.io/vectorized/redpanda:v21.11.2
    ```
* Run the tests
    ```bash
    ./mvnw test
    ```
  * the quarkus profile `test` is active
  * other profiles can be added with
  * ```bash
    ./mvnw test -Dquarkus.test.profile=test,my-profile
    ```   
  * when started from your IDE, testcontainers will act and start kafka, as well:
           ![img.png](img.png)


## Running tests with docker-compose provided-containers

As an alternative to testcontainers automatic, the kafka and postgresql containers can be started via docker-compose.
>Main difference to Testcontainers: the containers continue to run after the tests have.

The [](docker-compose.yaml) file refers to the specific images:

* ``image: docker.vectorized.io/vectorized/redpanda:v21.11.2``
* ``image: postgres:13.2``

Start the test containers:
* ```bash
  cd kafka-panache-quickstart
  docker-compose up  
  ```
Run the test:
* ```bash
  mvn test -Dquarkus.test.profile=notestservices
  ```
  * the profile `notestservices` is declared in  [src/main/resources/application.properties]() 
  * it defines the kafka bootstrap server
    ```bash
    %notestservices.kafka.bootstrap.servers=localhost:9092
    ```
    * this effectively disables testcontainers


Stop the docker containers:
* ```bash
  cd kafka-panache-quickstart
  docker-compose down
  ```

## Anatomy

* `PriceGenerator` - a bean generating random price. They are sent to a Kafka topic.
* `PriceStorage` - on the consuming side, the `PriceStorage` receives the Kafka message and write it into the database using Hibernate with Panache
* `PriceResource`  - the `PriceResource` retrieves the prices from the database and send them into the HTTP response

## Running in native

You can compile the application into a native binary using:

`./mvnw clean install -Pnative`

As you are not in dev or test mode, you need to start a PostgreSQL instance and a Kafka broker.
To start them, just run `docker-compose up -d`.

Then, run the application with:

`./target/kafka-panache-quickstart-1.0.0-SNAPSHOT-runner` 
